export default {
  props: {
    question: {
      type: Object,
      required: true
    }
  }
}
