import { mapActions, mapGetters } from 'vuex'

export default {
  components: {
    answer: () => import('@/components/Game/Answers/Answer')
  },
  props: {
    questionId: {
      type: [Number, String],
      required: true
    }
  },
  computed: {
    ...mapGetters([
      'questions'
    ]),
    isSavedQuestion () {
      return typeof this.questionId === 'number'
    },
    question () {
      let questionEmpty = { id: null, text: '', type: null, definition: {} }
      let question = this.questions.find(question => question.id === this.questionId)
      return question !== undefined ? question : questionEmpty
    }
  },
  methods: {
    ...mapActions([
      'changeDefinition',
      'changeText',
      'deleteQuestion',
      'saveQuestions'
    ]),
    onChangeLabel (label) {
      this.changeDefinition({ questionId: this.questionId, text: label, type: 'label'})
    },
    onChangeText (text) {
      this.changeText({ questionId: this.questionId, text })
    },
    onDeleteQuestion () {
      this.deleteQuestion(this.questionId)
    },
    onSaveQuestion () {
      this.saveQuestions()
    }
  }
}
