export const randomId = (min = null, max = null) => {
  if (min === null || max === null || min < 0 || min >= max || max > 20) {
    return Math.random().toString(36).substring(3, 8)
      + Math.random().toString(36).substring(4, 9)
      + Math.random().toString(36).substring(2, 7)
      + Math.random().toString(36).substring(5, 10)
  } else {
    return (Math.random().toString(36).substring(3, 8)
      + Math.random().toString(36).substring(4, 9)
      + Math.random().toString(36).substring(2, 7)
      + Math.random().toString(36).substring(5, 10)).substr(min, max)
  }
}
