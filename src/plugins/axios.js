'use strict'

import Vue from 'vue'
import axios from 'axios'

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config = {
  baseURL: process.env.VUE_APP_URL_API
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
}

export const _axios = axios.create(config)

const expired = token => {
  return JSON.parse(atob(token.split('.')[1])).exp < Math.trunc(Date.now() / 1000)
}

const login = () => {
  localStorage.clear()
  window.location = `${process.env.VUE_APP_URL_JWT_SERVER}connexion?redirectURL=${encodeURIComponent(window.btoa(process.env.VUE_APP_URL_FRONT + window.location.href))}`
}

const refresh = async () => {
  await axios({
    url: `${process.env.VUE_APP_URL_JWT_SERVER}api/refresh`,
    method: 'POST',
    data: { refresh: localStorage.getItem('JWT__refresh__token') }
  })
    .then(response => {
      localStorage.setItem('JWT__access__token', response.data.items.access)
    })
}

_axios.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    if (window.localStorage.getItem('JWT__access__token') !== null) {
      if (expired(window.localStorage.getItem('JWT__access__token'))) {
        if (window.localStorage.getItem('JWT__refresh__token') !== null) {
          if (expired(window.localStorage.getItem('JWT__refresh__token'))) {
            login()
          } else {
            await refresh()
          }
        } else {
          login()
        }
      }

      config.headers.Authorization = `Bearer ${window.localStorage.getItem('JWT__access__token')}`
    }

    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    return response
  },
  function (error) {
    // Do something with response error
    return Promise.reject(error)
  }
)

const Plugin = {}
Plugin.install = function (Vue) {
  Vue.axios = _axios
  window.axios = _axios
  Object.defineProperties(Vue.prototype, {
    axios: {
      get () {
        return _axios
      }
    },
    $axios: {
      get () {
        return _axios
      }
    }
  })
}

Vue.use(Plugin)

export default Plugin
