import Vue from 'vue'
import VueRouter from 'vue-router'
import { _axios } from '@/plugins/axios'

Vue.use(VueRouter)

const routes = [
  {
    path: '/admin/partie/creation',
    name: 'admin_game_create',
    component: () => import('@/views/Admin/Create'),
    meta: { Authorization: true }
  },
  {
    path: '/admin/partie/modification/:id',
    name: 'admin_game_update',
    component: () => import('@/views/Admin/Create'),
    meta: { Authorization: true }
  },
  {
    path: '/admin/partie/tableau-de-bord/:id',
    name: 'game_admin_dashboard_show',
    component: () => import('@/views/Admin/Dashboard'),
    meta: { Authorization: true }
  },
  {
    path: '/admin/partie/:id',
    name: 'admin_game_show',
    component: () => import('@/views/Admin/Show'),
    meta: { Authorization: true }
  },
  {
    path: '/admin/parties',
    name: 'admin_game_list',
    component: () => import('@/views/Admin/List'),
    meta: { Authorization: true }
  },
  {
    path: '/equipe/modification/:id',
    name: 'team_modification',
    component: () => import('@/views/User/Team/Create'),
    meta: { Authorization: true }
  },
  {
    path: '/equipe/creation',
    name: 'team_create',
    component: () => import('@/views/User/Team/Create'),
    meta: { Authorization: true }
  },
  {
    path: '/equipes',
    name: 'team_list',
    component: () => import('@/views/User/Team/List'),
    meta: { Authorization: true }
  },
  {
    path: '/partie/:partyId/equipe/:teamId/question/:questionId',
    name: 'game_question_pass',
    component: () => import('@/views/User/Game/QuestionView'),
    meta: { Authorization: true }
  },
  {
    path: '/partie/:partyId/equipe/:teamId',
    name: 'game_wait_page',
    component: () => import('@/views/User/Game/wait'),
    meta: { Authorization: true }
  },
  {
    path: '/parties',
    name: 'game_list',
    component: () => import('@/views/User/Game/List'),
    meta: { Authorization: true }
  },
  {
    path: '*',
    redirect: { name: 'game_list' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  if (to.meta.Authorization) {
    if (to.query.ticket) {
      await _axios({
        method: 'post',
        url: 'api/users',
        baseURL: process.env.VUE_APP_URL_JWT_SERVER,
        maxRedirects: 0,
        data: { ticket: to.query.ticket }
      })
        .then(response => {
          window.localStorage.setItem('JWT__access__token', response.data.items.access)
          window.localStorage.setItem('JWT__refresh__token', response.data.items.refresh)
          next(to.path)
          return true
        })
        .catch(error => {
          if (error.response.status === 401 && error.response.data.status === 200) {
            window.location = `${process.env.VUE_APP_URL_JWT_SERVER}connexion?redirectURL=${encodeURIComponent(window.btoa(process.env.VUE_APP_URL_FRONT + to.path))}`
          }
        })
      next()
    } else if (!window.localStorage.getItem('JWT__access__token')) {
      await _axios({
        method: 'get',
        url: 'api/users',
        baseURL: process.env.VUE_APP_URL_JWT_SERVER,
        maxRedirects: 0
      })
        .then(response => {
          window.localStorage.setItem('JWT__access__token', response.data.items.access)
          window.localStorage.setItem('JWT__refresh__token', response.data.items.refresh)
          next(to.path)
          return true
        })
        .catch(error => {
          if (error.response.status === 401 && error.response.data.status === 100) {
            window.location = `${process.env.VUE_APP_URL_JWT_SERVER}connexion?redirectURL=${encodeURIComponent(window.btoa(process.env.VUE_APP_URL_FRONT + to.path))}`
          }
        })
    }
  }

  next()
})

export default router
