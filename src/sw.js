/* eslint no-undef: 0 */
workbox.core.skipWaiting()
workbox.core.clientsClaim()

workbox.precaching.precacheAndRoute(self.__precacheManifest)

workbox.routing.registerRoute(
  /^(http:\/\/localhost:8000\/api\/admin\/games)$/,
  new workbox.strategies.NetworkFirst({
    plugins: [new workbox.cacheableResponse.Plugin({ statuses: [0, 200] })]
  }),
  'GET'
)
workbox.routing.registerRoute(
  /^(http:\/\/localhost:8000\/api\/admin\/games\/(\d*))$/,
  new workbox.strategies.NetworkFirst({
    plugins: [new workbox.cacheableResponse.Plugin({ statuses: [0, 200] })]
  }),
  'GET'
)
workbox.routing.registerRoute(
  /(.(jpg|jpeg|png))$/,
  new workbox.strategies.CacheFirst({
    plugins: [new workbox.cacheableResponse.Plugin({ statuses: [0, 200] })]
  }),
  'GET'
)
// workbox.routing.registerRoute(
//   /^(https:\/\/api.jojotique.fr\/api\/admin\/games)$/,
//   new workbox.strategies.StaleWhileRevalidate({
//     plugins: [new workbox.cacheableResponse.Plugin({ statuses: [0, 200] })]
//   }),
//   'GET'
// )
// workbox.routing.registerRoute(
//   /^(https:\/\/api.jojotique.fr\/api\/admin\/games\/(\d*))$/,
//   new workbox.strategies.StaleWhileRevalidate({
//     plugins: [new workbox.cacheableResponse.Plugin({ statuses: [0, 200] })]
//   }),
//   'GET'
// )
