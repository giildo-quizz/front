import Vue from 'vue'
import Vuex from 'vuex'

import Games from './modules/game'
import Party from './modules/party'
import Questions from './modules/question'
import Teams from './modules/team'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    Games,
    Party,
    Questions,
    Teams
  }
})
