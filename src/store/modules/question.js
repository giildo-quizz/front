import { _axios } from '@/plugins/axios'
import { randomId } from '@/plugins/randomId'

const convertQuestionType = type => {
  switch (type) {
    case 'image':
      return 1
    case 'video':
      return 2
    case 'text':
      return 3
  }
}

/**
 * @typedef {Object} QuestionState
 * @property {Question} question - Stockage de la question en cours pour les utilisateurs
 * @property {Question[]} questions - Stockage de toutes les questions d'une partie
 * @type {QuestionState}
 */
const state = {
  question: null,
  /**
   * @typedef {Object} Answer
   * @property {String} id - Identifiant de la proposition de réponse
   * @property {String} text - Texte de la proposition de réponse
   */
  /**
   * @typedef {Object} AnswerSettings
   * @property {String} type - Indique si c'est des réponses de type "QCM/QCU" ou "ouverte"
   * @property {Boolean} qcu - Pour les réponses de type "QCM/QCU" pour stocker si c'est des QCU ou des QCM
   * @property {Number} openNumberFields - Pour les réponses de type "ouverte" pour stocker le nombre de champs
   * @property {Answer[]} answers - Date de création de la partie
   */
  /**
   * @typedef {Object} QuestionDefinition
   * @property {String} label - Pour les questions de type image ou vidéo pour ajouter un label au média
   * @property {String} fileName - Champ pour les question de type image pour enregistrer le nom de l'image
   * @property {File|Object} file - Champ pour les question de type image pour enregistrer l'image
   * @property {AnswerSettings} answerSettings - Objet pour enregistrer les options des types de réponse
   */
  /**
   * @typedef {Object} Question
   * @property {Number} id - Identifiant de la question
   * @property {String} text - Texte de la question
   * @property {String|Number} type - Type de la question (converter ci-dessus)
   * @property {QuestionDefinition} definition - JSON pour la définition de la question
   */
  questions: [],
  results: []
}

const getters = {
  question: state => state.question,
  questions: state => state.questions,
  results: state => state.results
}

const actions = {
  addQuestion: ({ commit }, payload) => {
    commit('ADD_QUESTION', payload)
  },
  addQuestionToInstance: async (context, { questionId, instanceId }) => {
    await _axios.patch(`/instance/${instanceId}/questions/${questionId}`)
  },
  changeText: ({ commit }, payload) => {
    commit('CHANGE_TEXT', payload)
  },
  changeDefinition: ({ commit }, payload) => {
    commit('CHANGE_DEFINITION', payload)
  },
  changeAnswerType: ({ commit }, payload) => {
    commit('CHANGE_ANSWER_TYPE', payload)
  },
  deleteQuestion: async ({ commit, state }, questionId) => {
    const questionToDelete = state.questions.find(question => question.id === questionId)

    if (questionToDelete.type === 'image' && typeof questionId !== 'string') {
      await _axios.post(`/admin/questions/files/delete`, { fileName: questionToDelete.definition.fileName })
    }

    typeof questionId === 'string'
      ? commit('DELETE_QUESTION', questionId)
      : await _axios.delete(`/admin/questions/${questionId}`)
        .then(() => {
          commit('DELETE_QUESTION', questionId)
        })
  },
  loadAnswer: async ({ commit }, { teamId, partyId, questionId }) => {
    await _axios.get(`/answers/team/${teamId}/party/${partyId}/question/${questionId}`)
      .then(response => {
        if (response.data.item !== null) commit('SAVE_ANSWER', response.data.item)
      })
  },
  loadAnswers: async ({ commit }, instanceId) => {
    await _axios.get(`/answers/instance/${instanceId}`)
      .then(response => {
        commit('SAVE_RESULTS', response.data.items)
      })
  },
  loadQuestion: async ({ commit }, questionId) => {
    await _axios.get(`/questions/${questionId}`)
      .then(response => {
        commit('LOAD_QUESTION', response.data.item)
      })
  },
  openChangeNumberField: ({ commit }, payload) => {
    commit('OPEN_CHANGE_NUMBER_FIELD', payload)
  },
  mcqAddAnswer: ({ commit }, questionId) => {
    commit('MCQ_ADD_ANSWER', questionId)
  },
  mcqChangeMcqUcq: ({ commit }, payload) => {
    commit('MCQ_CHANGE_MCQ_UCQ', payload)
  },
  mcqChangeAnswerText: ({ commit }, payload) => {
    commit('MCQ_CHANGE_ANSWER_TEXT', payload)
  },
  mcqDeleteAnswer: ({ commit }, payload) => {
    commit('MCQ_DELETE_ANSWER', payload)
  },
  saveQuestions: async ({ commit, state, getters }) => {
    for (const question of state.questions) {
      const questionSave = { ...question }
      const oldId = question.id
      questionSave.type = convertQuestionType(questionSave.type)
      questionSave.game = getters.game.id

      if (question.definition.file !== undefined && question.definition.file.name !== undefined) {
        let formData = new FormData()

        if (typeof questionSave.id !== 'string') {
          await _axios.post(`/admin/questions/files/delete`, { fileName: questionSave.definition.fileName })
        }

        questionSave.definition.fileName = randomId() + '.' + question.definition.file.type.replace(/image\/(jpg|jpeg|png|bmp)$/, '$1')

        formData.append('files', questionSave.definition.file, questionSave.definition.fileName)

        await _axios.post('/admin/questions/files', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
      }

      questionSave.definition = JSON.stringify(questionSave.definition)
      let response = typeof questionSave.id === 'string'
        ? await _axios.post('/admin/questions', questionSave)
        : await _axios.patch(`/admin/questions/${questionSave.id}`, questionSave)

      response.data.item.definition = JSON.parse(response.data.item.definition)
      commit('UPDATE_QUESTION', { questionUpdated: response.data.item, oldId })
    }
  },
  sendAnswer: async ({ commit }, payload) => {
    await _axios.post(`/answers`, payload)
      .then(response => {
        commit('SAVE_ANSWER', response.data.item)
      })
  }
}

const mutations = {
  ADD_QUESTION: (state, { id, type }) => {
    let definition = {
      answerSettings: {
        type: '',
        answers: [],
        answerValue: ''
      }
    }

    if (type === 'image') {
      definition.label = ''
      definition.file = null
      definition.fileName = ''
    } else if (type === 'video') {
      definition.url = ''
      definition.label = ''
    }

    state.questions.push({
      id,
      text: '',
      type,
      definition
    })
  },
  DELETE_QUESTION: (state, questionId) => {
    state.questions = state.questions.filter(question => question.id !== questionId)
  },
  CHANGE_ANSWER_TYPE: (state, { questionId, questionType }) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.type = questionType
  },
  CHANGE_DEFINITION: (state, { questionId, text, type }) => {
    state.questions.find(question => question.id === questionId).definition[type] = text
  },
  CHANGE_TEXT: (state, { questionId, text }) => {
    state.questions.find(question => question.id === questionId).text = text
  },
  LOAD_QUESTION: (state, question) => {
    let questionLocal = Object.assign({}, question)
    state.question = questionLocal
    state.question.definition = JSON.parse(questionLocal.definition)
  },
  LOAD_QUESTIONS: (state, questions) => {
    state.questions = questions
  },
  OPEN_CHANGE_NUMBER_FIELD: (state, { questionId, number }) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.openNumberFields = number
  },
  MCQ_ADD_ANSWER: (state, questionId) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.answers.push({
      id: randomId(),
      text: ''
    })
  },
  MCQ_CHANGE_MCQ_UCQ: (state, { questionId, isQcu }) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.qcu = isQcu
  },
  MCQ_CHANGE_ANSWER_TEXT: (state, { questionId, answerId, value }) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.answers.find(answer => answer.id === answerId).text = value
  },
  MCQ_DELETE_ANSWER: (state, { questionId, answerId }) => {
    state.questions.find(question => question.id === questionId).definition.answerSettings.answers = state.questions.find(question => question.id === questionId).definition.answerSettings.answers.filter(answer => answer.id !== answerId)
  },
  SAVE_ANSWER: (state, { value }) => {
    state.question.definition.answerSettings.answerValue = value
  },
  SAVE_RESULTS: (state, results) => {
    state.results = results
  },
  UPDATE_QUESTION: (state, { questionUpdated, oldId }) => {
    state.questions = state.questions.map(question => {
      if (question.id === oldId) {
        question = { ...questionUpdated }
      }

      return question
    })
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}
