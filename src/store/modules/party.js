import { _axios } from '@/plugins/axios'
import router from '@/router'


const state = {
  party: {},
  partyTeams: []
}

const getters = {
  party: state => state.party,
  partyTeams: state => state.partyTeams
}

const actions = {
  checkNextQuestion: async({ commit }, { partyId, instanceId, teamId, questionId }) => {
    await _axios.get(`/parties/${partyId}/instance/${instanceId}/question/${questionId}`)
      .then(response => {
        if (response.status === 200) {
          commit('LOAD_QUESTION', response.data.item)
          router.push({
            name: 'game_question_pass',
            params: {
              questionId: response.data.item.id,
              partyId,
              teamId
            }
          })
        }
      })
  },
  createParty: async({ commit }, payload) => {
    await _axios.post('/parties', payload)
      .then(response => {
        commit('CREATE_PARTY', response.data.item)
      })
  },
  loadParty: async({ commit }, partyId) => {
    if (state.party.id !== partyId) {
      await _axios.get(`/parties/${partyId}`)
        .then(response => {
          commit('CREATE_PARTY', response.data.item)
        })
    }
  },
  loadPartyTeams: async({ commit }, instanceId) => {
    await _axios.get(`/parties/${instanceId}/teams`)
      .then(response => {
        commit('LOAD_PARTY_TEAMS', response.data.items)
      })
  }
}

const mutations = {
  CREATE_PARTY: (state, party) => {
    state.party = party
  },
  LOAD_PARTY_TEAMS: (state, parties) => {
    state.partyTeams.splice(0, state.partyTeams.length)
    parties.forEach(party => {
      state.partyTeams.push(party.team)
    })
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}
