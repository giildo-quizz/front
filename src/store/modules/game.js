import { _axios } from '@/plugins/axios'
import router from '@/router'

/**
 * @typedef {Object} GameState
 * @property {Game} game - Stockage d'une partie
 * @property {Game[]} games - Stockage de toutes les parties
 * @property {Game[]} gamesPublished - Stockage de toutes les parties publiées
 * @type {GameState}
 */
const state = {
  game: { id: null, title: '', description: '', createdAt: null, updatedAt: null, questions: [] },
  /**
   * @typedef {Object} Game
   * @property {Number} id - Identifiant de la partie
   * @property {String} title - Titre de la partie
   * @property {String} description - Description de la partie
   * @property {Date} createdAt - Date de création de la partie
   * @property {Date} updatedAt - Date de la dernière modification de la partie
   * @property {Boolean} closed - Questionnaire clôturer
   * @property {Boolean} published - Questionnaire publié
   * @property {Boolean} started - Questionnaire est démarré
   * @property {Question[]} questions - Questions rattachées à la partie
   */
  games: [],
  gamesPublished: [],
  instance: null
}

const getters = {
  game: state => state.game,
  games: state => state.games,
  gamesPublished: state => state.gamesPublished,
  instance: state => state.instance
}

const actions = {
  changePublishStatus: async ({ commit, state }, gameId) => {
    const gameToChangeStatus = state.game.id === gameId
      ? state.game
      : state.games.find(game => game.id === gameId)

    await _axios.post(`/admin/games/${gameId}/publish`, { published: !gameToChangeStatus.published })
      .then(response => {
        commit('UPDATE_GAME', response.data.item)
      })
  },
  closeGame: async ({ commit }, gameId) => {
    await _axios.delete(`/admin/games/${gameId}`)
      .then(() => {
        commit('DELETE_GAME', gameId)
      })
  },
  /**
   * @param {ActionContext} context
   * @param {Commit} context.commit
   * @param {Object} game
   * @param {String} game.title.text - Texte de la partie
   * @param {String} game.description.text - Description de la partie
   * @return {Promise<void>}
   */
  createGame: async ({ commit, state }, game) => {
    await _axios.post('/admin/games', {
      title: game.title.text,
      description: game.description.text
    })
      .then(response => {
        commit('CREATE_GAME', response.data.item)
        router.push({ name: 'admin_game_show', params: { id: state.game.id } })
      })
  },
  loadGame: async ({ commit, state }, gameId) => {
    let gameSaved = state.games.find(game => game.id === gameId)

    if (gameSaved) {
      commit('LOAD_GAME', { game: gameSaved, oneGame: true })
      commit('LOAD_QUESTIONS', gameSaved.questions)
    } else {
      await _axios.get(`/admin/games/${gameId}`)
        .then(response => {
          let game = response.data.item
          game.questions.map(question => {
            question.definition = JSON.parse(question.definition)
            return question
          })
          commit('LOAD_GAME', { game: response.data.item, oneGame: true })
          commit('LOAD_QUESTIONS', response.data.item.questions)
        })
    }
  },
  loadGames: async ({ commit }, published = false) => {
    published
      ? await _axios.get('/admin/games/published')
        .then(response => {
          commit('LOAD_GAME', { gamesPublished: response.data.items, published: true })
        })
      : await _axios.get('/admin/games')
        .then(response => {
          response.data.items.forEach(game => {
            game.questions = game.questions.map(question => {
              question.definition = JSON.parse(question.definition)
              return question
            })
          })
          commit('LOAD_GAME', { games: response.data.items, oneGame: false })
        })
  },
  startGame: async({ commit }, id) => {
    await _axios.patch(`/admin/games/${id}/start`)
      .then(response => {
        commit('LOAD_INSTANCE', response.data.item)
        commit('UPDATE_GAME', response.data.item.game)
      })
  },
  stopGame: async({ commit }, id) => {
    await _axios.patch(`/admin/games/${id}/stop`)
      .then(response => {
        commit('UPDATE_GAME', response.data.item)
      })
  },
  updateGame: async (context, { gameId, description, title }) => {
    await _axios.patch(`/admin/games/${gameId}`, { description, title })
      .then(() => {
        router.push({ name: 'admin_game_list' })
      })
  }
}

const mutations = {
  CREATE_GAME: (state, game) => {
    state.games.push(game)
    state.game = game
    state.questions = game.questions
  },
  DELETE_GAME: (state, gameId) => {
    state.games = state.games.filter(game => game.id !== gameId)
  },
  LOAD_INSTANCE: (state, instance) => {
    state.instance = instance
  },
  LOAD_GAME: (state, { game, games, gamesPublished, oneGame, published }) => {
    published
      ? state.gamesPublished = gamesPublished
      : oneGame
        ? state.game = game
        : state.games = games
  },
  UPDATE_GAME: (state, gameUpdated) => {
    state.games = state.games.map(game => game.id === gameUpdated.id ? gameUpdated : game)
    state.gamesPublished = state.gamesPublished.map(game => game.id === gameUpdated.id ? gameUpdated : game)
    state.game = { ...gameUpdated }
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}
