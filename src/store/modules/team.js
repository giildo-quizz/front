import { _axios } from '@/plugins/axios'

/**
 * @typedef {Object} TeamState
 * @property {Team} team - Équipe sélectionnée
 * @property {Team[]} teams - Liste des équipes
 * @type {TeamState}
 */
const state = {
  /**
   * @typedef {Object} Team
   * @property {Number} id - Identifiant de l'équipe
   * @property {String} name - Nom de l'équipe
   * @property {String[]} gamers - Nom des membres de l'équipe
   * @type {Team}
   */
  team: {
    id: null,
    name: '',
    gamers: []
  },
  /**
   * @type {Team[]}
   */
  teams: []
}

const getters = {
  team: state => state.team,
  teams: state => state.teams
}

const actions = {
  createTeam: async ({ commit }, team) => {
    await _axios.post(`/teams`, team)
      .then(response => {
        commit('CREATE_TEAM', response.data.item)
      })
  },
  deleteTeam: async ({ commit }, id) => {
    await _axios.delete(`/teams/${id}`)
      .then(() => {
        commit('DELETE_TEAM', id)
      })
  },
  loadTeam: async ({ commit }, id = null) => {
    id === null
      ? await _axios.get(`/teams`)
        .then(response => {
          commit('LOAD_TEAM', { teams: response.data.items, unique: false })
        })
      : await _axios.get(`/teams/${id}`)
        .then(response => {
          commit('LOAD_TEAM', { team: response.data.item, unique: true })
        })
  },
  updateTeam: async ({ commit }, { team, id }) => {
    await _axios.patch(`/teams/${id}`, team)
      .then(response => {
        commit('UPDATE_TEAM', response.data.item)
      })
  }
}

const mutations = {
  CREATE_TEAM: (state, team) => {
    state.team = team
    state.teams.push(team)
  },
  DELETE_TEAM: (state, id) => {
    state.teams.splice(state.teams.findIndex(team => team.id === id), 1)
  },
  LOAD_TEAM: (state, { teams, team, unique }) => {
    unique ? state.team = team : state.teams = teams
  },
  UPDATE_TEAM: (state, teamUpdated) => {
    state.teams = state.teams.map(team => team.id === teamUpdated.id ? teamUpdated : team)
    state.team = { ...teamUpdated }
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}
