import moxios from 'moxios'
import sinon from 'sinon'
import { expect } from 'chai'

import { _axios } from '@/plugins/axios'
import store from '@/store'
import { gameExample } from '../fixtures/GameFixtures'

describe('store/game', () => {
  let axiosSpyGet = null

  beforeEach(() => {
    moxios.install(_axios)
    axiosSpyGet = sinon.spy(_axios, 'get')
  })

  afterEach(() => {
    _axios.get.restore()
    moxios.uninstall(_axios)
  })

  describe('Create game', () => {
    it('should load the games', done => {
      moxios.stubRequest(`${_axios.defaults.baseURL}/games`, {
        status: 200,
        response: {items: [gameExample]}
      })

      store.dispatch('Games/loadGames')

      moxios.wait(() => {
        expect(axiosSpyGet.called).to.be.true
        expect(store.getters['Games/games'][0].id).to.be.equal(gameExample.id)
        done()
      })
    })
  })
})
