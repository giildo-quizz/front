export const gameExample = {
  id: 1,
  title: 'Partie d\'image',
  description: 'Description de la partie',
  questions: [
    {
      id: 1,
      question: 'Quelle est la star sur cette image',
      definition: {
        imageName: 'dsqdsqdzd.jpg'
      }
    },
    {
      id: 2,
      question: 'Quelle est la star sur cette image',
      definition: {
        imageName: 'dsqdzavreg.jpg'
      }
    },
    {
      id: 3,
      question: 'Quelle est la star sur cette image',
      definition: {
        imageName: 'lliokyujk.jpg'
      }
    }
  ]
}
