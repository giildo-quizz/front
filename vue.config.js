const { InjectManifest } = require('workbox-webpack-plugin')

module.exports = {
  'filenameHashing': true,
  'transpileDependencies': [
    'vuetify'
  ],
  configureWebpack: {
    plugins: [
      new InjectManifest({
        swSrc: './src/sw.js',
        swDest: 'service-worker.js'
      })
    ]
  }
}
